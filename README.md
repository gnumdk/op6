# Fedora Mobile Remix for OnePlus 6

- Disclaimer: Use this at your own risk
- Very much still a Work in Progress - relies on postmarketOS boot images for example.
- Note: This is a snapshot of Markus' work at https://www.maggu2810.de/mobile-devices/op6-instructions-fedora.html and https://github.com/maggu2810/op6-containers/tree/main/fedora, who got this method working. See updates there. Republished here with permission. 
- Another great writeup is Justin's, which is based on Markus' method and but doesn't push to a registry: https://www.opencode.net/justinz/oneplus6-fedora-image

## How to make a bootable image and flash it to your phone

### download pre-built container with relevant DE

Phosh:
```
podman pull docker.io/maggu2810/op6-fedora-phosh:latest
podman container create --arch "aarch64" -it --name "op6-fedora-phosh" "docker.io/maggu2810/op6-fedora-phosh:latest"
```
KDE:
```
podman pull docker.io/maggu2810/op6-fedora-plasma-mobile:latest
podman container create --arch "aarch64" -it --name "op6-fedora-plasma-mobile" "docker.io/maggu2810/op6-fedora-plasma-mobile:latest"
```

### clean up & create empty image

```
export IMG_PATH="${PWD}/oneplus-enchilada-fedora.img"
rm -rf "${IMG_PATH}"
truncate -s 5G "${IMG_PATH}"


export DEV_IMG="$(sudo losetup -P -f "${IMG_PATH}" -b 4096 --show)"
```

### create partitions

```
sudo parted -s "${DEV_IMG}" mktable msdos
sudo parted -s "${DEV_IMG}" mkpart primary ext2 2048s 256M
sudo parted -s "${DEV_IMG}" mkpart primary 256M 100%
sudo parted -s "${DEV_IMG}" set 1 boot on

export DEV_BOOT="${DEV_IMG}p1"
export DEV_ROOT="${DEV_IMG}p2"
```

### format partitions

```
sudo mkfs.ext4 -O ^metadata_csum -F -q -L pmOS_root -N 100000 "${DEV_ROOT}"
sudo mkfs.ext2 -F -q -L pmOS_boot "${DEV_BOOT}"
```

### mount fs

```
mkdir -p mnt
sudo mount "${DEV_ROOT}" mnt
sudo mkdir -p mnt/boot
sudo mount "${DEV_BOOT}" mnt/boot
```

### unpack container

```
podman export op6-fedora-phosh | sudo tar -C mnt/ -xp
```

### copy boot image

```
cp mnt/boot/boot.img boot.img
```

### umount & remove loop device

```
sudo umount mnt/boot
sudo umount mnt
sudo sync
sudo losetup -d "${DEV_IMG}"
sudo sync
```

### create android sparse image

```
img2simg oneplus-enchilada-fedora.{img,simg}
sudo sync
mv oneplus-enchilada-fedora.{simg,img}
```

### flash

```
#fastboot erase --slot=all system
#fastboot erase userdata
#fastboot erase --slot=all boot
sudo fastboot erase dtbo
sudo fastboot flash boot --slot=all boot.img
sudo fastboot flash userdata oneplus-enchilada-fedora.img

```


## How to update the container images

Run `containerfiles/build-and-push-all.sh` on an ARM machine (or an x86 machine if you want terrible cross-compilation performance) to build the base containers.

Push them to (currently) Markus' Docker for consumption in the step above. Eventually they'll be pushed to https://registry.fedoraproject.org

## Credits

- This image was inspired by [Jonas Dreßler](https://gitlab.gnome.org/verdre/)'s work with the help of [Caleb](https://floss.social/@calebccff@fosstodon.org) on the [Alarm OP6 repo](https://gitlab.gnome.org/sonny/op6).
- Thanks to Caleb and the postmarketOS community for supporting the OnePlus 6
- Thanks to Linaro for [mainlining the Qualcomm Snapdragon](https://www.linaro.org/blog/let-s-boot-the-mainline-linux-kernel-on-qualcomm-devices/) platform.
- Thanks to Purism for investing in the GNOME ecosystem.
- Everyone else involved in making Linux mobile possible.


## Demo

![](containerfiles/helpers/fedora-op6-enchilada-phosh.jpg) ![](containerfiles/helpers/fedora-op6-enchilada-kde.jpg)